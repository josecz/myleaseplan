Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario Outline: Search for the product existing product should return results of that product
    When user calls endpoint <url> for given <product>
    Then user sees the results displayed for given <product>
    Examples:
      | url | product |
      | url | apple   |
      | url | mango   |
      | url | tofu    |
      | url | water   |

  Scenario Outline: Search for the product non-existing product should not return results
    When user calls endpoint <url> for given <product>
    Then user does not see the results <results>
    Examples:
      | url | product | results |
      | url | car     | true    |
      | url | bike    | true    |


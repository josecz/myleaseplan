package com.leaseplan.utilis;
/**
 * This class is a utility class to read priorities file from class path as resource stream
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReaderUtils {


    InputStream inputStream;

    Properties props = new Properties();
    String propFileName = "props.properties";


    /**
     * read file as resource stream
     */

    public void readFile() {

        try {

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                props.load(inputStream);
            } else {
                throw new FileNotFoundException(
                        "property file '" + propFileName + "' not found in the classpath");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * read single property
     *
     * @param prop
     * @return property value
     */
    public String getPropValue(String prop) {
        readFile();
        return props.getProperty(prop);
    }

}

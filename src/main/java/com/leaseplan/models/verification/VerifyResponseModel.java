package com.leaseplan.models.verification;
/**
 * model class to verify api response
 */

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class VerifyResponseModel {
    /**
     * verify api response method
     *
     * @param statuCode
     * @param path
     * @param product
     */
    public void verifyResponse(int statuCode, String path, String product) {
        restAssuredThat(response ->
                response.statusCode(statuCode).extract().jsonPath().getJsonObject(path).toString().equalsIgnoreCase(product));

    }
}

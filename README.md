# Setting up the test project
The framework used is Java, RESTAssured and Serenity BBD and Maven for building and running the test project.
in order to install the project, first clone the project to your local repo, once cloning finished.

* in terminal type `mvn clean install` in order to build 
and compile the project.

* for running the test use command `mvn verify` at the end of of the build a full test summary report will be generated **[target/site/serenity/index.html]** 

* writing new tests, the project use Cucumber (Gherkin) scenario outlines for example search for a product
```Gherkin
  Scenario Outline: Search for the product existing product should return results of that product
    When user calls endpoint <url> for given <product>
    Then user sees the results displayed for given <product>
    Examples:
      | url | product |
      | url | apple   |
      | url | mango   |
```
you may copy above scenario outline and modify it based on your test case need.

### The project directory structure
The project has build scripts for both Maven and Gradle, and follows the standard directory structure used in most Serenity projects:
```Gherkin
src
  + main
  + test
    + java                        Test runners and supporting code
    + resources
      + features                  Feature files
      + properties                Properties file
    + search                  Feature file subdirectories
             post_product.feature
```

## what has been refactored?
* code and file cleaning for  unused codes and files (CarsAPI.Java for example) 
* removed hardcoded URL from the test file to property file (which it could be use as test data file for our the feature files)
* reworked some RestAssured check on the API response as it was not working
* added scenario out lines to the tests
* also removed from pom.xml some unused dependencies and webdriver setting as they are not needed for API integration tests
* add test models for example verify response model
* and some method proper renaming

